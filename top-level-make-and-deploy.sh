#!/bin/bash
set -x

user=mabraham

deploymentlocation=${user}@manual.gromacs.org:/var/www/manual

# Re-make the top-level documentation for all versions
make html

# Ensure useful read permissions when placing on a server
upload="rsync -avP --chmod=u+rwX,g+rwX,o+rX"
$upload -av _build/html/ $deploymentlocation/ --exclude _sources --exclude .buildinfo --exclude objects.inv

