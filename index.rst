Welcome to the GROMACS documentation!
=================================================

Latest releases
----------------------

* GROMACS 2025 series

    `Documentation <current/index.html>`__ for the current version

    * 2025.0 released February 11th, 2025
        + `Documentation <2025.0/index.html>`__
        + `Download <2025.0/download.html>`__
        + `Release Notes <2025.0/release-notes/index.html>`__



Maintained releases
-----------------------------------------------------------

* GROMACS 2024 series

    `Documentation <2024-current/index.html>`__ for the current version

    * 2024.5 released January 24th, 2025
        + `Documentation <2024.5/index.html>`__
        + `Download <2024.5/download.html>`__
        + `Release Notes <2024.5/release-notes/index.html>`__
    * 2024.4 released October 31st, 2024
        + `Documentation <2024.4/index.html>`__
        + `Download <2024.4/download.html>`__
        + `Release Notes <2024.4/release-notes/index.html>`__
    * 2024.3 released August 29th, 2024
        + `Documentation <2024.3/index.html>`__
        + `Download <2024.3/download.html>`__
        + `Release Notes <2024.3/release-notes/index.html>`__
    * 2024.2 released May 10th, 2024
        + `Documentation <2024.2/index.html>`__
        + `Download <2024.2/download.html>`__
        + `Release Notes <2024.2/release-notes/index.html>`__
    * 2024.1 released February 28th, 2024
        + `Documentation <2024.1/index.html>`__
        + `Download <2024.1/download.html>`__
        + `Release Notes <2024.1/release-notes/index.html>`__
    * 2024.0 released January 30th, 2024
        + `Documentation <2024.0/index.html>`__
        + `Download <2024.0/download.html>`__
        + `Release Notes <2024.0/release-notes/index.html>`__


Current development branch
-----------------------------------------------------------

This page is linked for the convenience of current GROMACS developers.
Normal users should refer to the documentation for their numbered
version.

`Latest main branch documentation <nightly/index.html>`__

Releases no longer being maintained
-----------------------------------------------------------

Those branches are no longer maintained, please see 
:ref:`here <branch-maintenance-policy>` for an explanation.

* GROMACS 2023 series

    `Documentation <2023-current/index.html>`__ for the current version
    
    * 2023.5 released May 3rd, 2024
        + `Download <2023.5/download.html>`__
        + `Release Notes <2023.5/release-notes/2023/2023.5.html>`__
    * 2023.4 released January 24th, 2024
        + `Download <2023.4/download.html>`__
        + `Release Notes <2023.4/release-notes/2023/2023.4.html>`__
    * 2023.3 released October 19th, 2023
        + `Download <2023.3/download.html>`__
        + `Release Notes <2023.3/release-notes/2023/2023.3.html>`__
    * 2023.2 released July 12th, 2023
        + `Download <2023.2/download.html>`__
        + `Release Notes <2023.2/release-notes/2023/2023.2.html>`__
    * 2023.1 released April 21st, 2023
        + `Download <2023.1/download.html>`__
        + `Release Notes <2023.1/release-notes/2023/2023.1.html>`__
    * 2023 released February 6th, 2023
        + `Download <2023/download.html>`__
        + `Release Notes <2023/release-notes/index.html>`__

* GROMACS 2022 series

    `Documentation <2022-current/index.html>`__ for the current version

    * 2022.6 released July 11th, 2023
        + `Download <2022.6/download.html>`__
        + `Release Notes <2022.6/release-notes/2022/2022.6.html>`__
    * 2022.5 released February 3rd, 2023
        + `Download <2022.5/download.html>`__
        + `Release Notes <2022.5/release-notes/2022/2022.5.html>`__
    * 2022.4 released November 16th, 2022
        + `Download <2022.4/download.html>`__
        + `Release Notes <2022.4/release-notes/2022/2022.4.html>`__
    * 2022.3 released September 2nd, 2022
        + `Download <2022.3/download.html>`__
        + `Release Notes <2022.3/release-notes/2022/2022.3.html>`__
    * 2022.2 released June 16th, 2022
        + `Download <2022.2/download.html>`__
        + `Release Notes <2022.2/release-notes/2022/2022.2.html>`__
    * 2022.1 released April 22nd, 2022
        + `Download <2022.1/download.html>`__
        + `Release Notes <2022.1/release-notes/2022/2022.1.html>`__
    * 2022 released February 22, 2022
        + `Download <2022/download.html>`__
        + `Release Notes <2022/release-notes/index.html>`__

* GROMACS 2021 series

    `Documentation <2021-current/index.html>`__ for the current version

    * 2021.7 released January 31st, 2023
        + `Download <2021.7/download.html>`__
        + `Release Notes <2021.7/release-notes/2021/2021.7.html>`__
    * 2021.6 released July 8th, 2022
        + `Download <2021.6/download.html>`__
        + `Release Notes <2021.6/release-notes/2021/2021.6.html>`__
    * 2021.5 released January 14th, 2022
        + `Download <2021.5/download.html>`__
        + `Release Notes <2021.5/release-notes/2021/2021.5.html>`__
    * 2021.4 released November 5th, 2021
        + `Download <2021.4/download.html>`__
        + `Release Notes <2021.4/release-notes/2021/2021.4.html>`__
    * 2021.3 released August 18th, 2021
        + `Download <2021.3/download.html>`__
        + `Release Notes <2021.3/release-notes/2021/2021.3.html>`__
    * 2021.2 released May 5th, 2021
        + `Download <2021.2/download.html>`__
        + `Release Notes <2021.2/release-notes/2021/2021.2.html>`__
    * 2021.1 released March 8th, 2021
        + `Download <2021.1/download.html>`__
        + `Release Notes <2021.1/release-notes/2021/2021.1.html>`__
    * 2021 released January 28th, 2021
        + `Download <2021/download.html>`__
        + `Release Notes <2021/release-notes/index.html>`__

* GROMACS 2020 series

    `Documentation <2020-current/index.html>`__ for the current version

    * 2020.7 released February 3rd, 2022
        + `Download <2020.7/download.html>`__
        + `Release Notes <2020.7/release-notes/2020/2020.7.html>`__
    * 2020.6 released March 4th, 2021
        + `Download <2020.6/download.html>`__
        + `Release Notes <2020.6/release-notes/2020/2020.6.html>`__
    * 2020.5 released January 6th, 2021
        + `Download <2020.5/download.html>`__
        + `Release Notes <2020.5/release-notes/2020/2020.5.html>`__
    * 2020.4 released October 6th, 2020
        + `Download <2020.4/download.html>`__
        + `Release Notes <2020.4/release-notes/2020/2020.4.html>`__
    * 2020.3 released July 9th, 2020
        + `Download <2020.3/download.html>`__
        + `Release Notes <2020.3/release-notes/2020/2020.3.html>`__
    * 2020.2 released April 30th, 2020
        + `Download <2020.2/download.html>`__
        + `Release Notes <2020.2/release-notes/2020/2020.2.html>`__
    * 2020.1 released March 3rd, 2020
        + `Download <2020.1/download.html>`__
        + `Release Notes <2020.1/release-notes/2020/2020.1.html>`__
    * 2020 released January 1st, 2020
        + `Download <2020/download.html>`__
        + `Release Notes <2020/release-notes/index.html>`__

* GROMACS 2019 series

    `Documentation <2019-current/index.html>`__ for the current version

    * 2019.6 released February 28th, 2020                                 
        + `Download <2019.6/download.html>`__
        + `Release Notes <2019.6/release-notes/2019/2019.6.html>`__
    * 2019.5 released December 23th, 2019                                 
        + `Download <2019.5/download.html>`__
        + `Release Notes <2019.5/release-notes/2019/2019.5.html>`__
    * 2019.4 released October 2nd, 2019
        + `Download <2019.4/download.html>`__
        + `Release Notes <2019.4/release-notes/2019/2019.4.html>`__
    * 2019.3 released June 14th, 2019
        + `Download <2019.3/download.html>`__
        + `Release Notes <2019.3/release-notes/2019/2019.3.html>`__
    * 2019.2 released April 16th, 2019
        + `Download <2019.2/download.html>`__
        + `Release Notes <2019.2/release-notes/2019/2019.2.html>`__
    * 2019.1 released February 15th, 2019
        + `Download <2019.1/download.html>`__
        + `Release Notes <2019.1/release-notes/2019/2019.1.html>`__
    * 2019 released December 31st, 2018
        + `Download <2019/download.html>`__
        + `Release Notes <2019/release-notes/index.html>`__

* GROMACS 2018 series

    `Documentation <2018-current/index.html>`__ for the latest version.

    * 2018.8 released October 4th, 2019
        + `Download <2018.8/download.html>`__
        + `Release Notes <2018.8/release-notes/2018/2018.8.html>`__
    * 2018.7 released May 29th, 2019
        + `Download <2018.7/download.html>`__
        + `Release Notes <2018.7/release-notes/2018/2018.7.html>`__
    * 2018.6 released February 22nd, 2019
        + `Download <2018.6/download.html>`__
        + `Release Notes <2018.6/release-notes/2018/2018.6.html>`__
    * 2018.5 released January 22nd, 2019
        + `Download <2018.5/download.html>`__
        + `Release Notes <2018.5/release-notes/2018/2018.5.html>`__
    * 2018.4 released November 12th, 2018
        + `Download <2018.4/download.html>`__
        + `Release Notes <2018.4/release-notes/2018/2018.4.html>`__
    * 2018.3 released August 23rd, 2018
        + `Download <2018.3/download.html>`__
        + `Release Notes <2018.3/release-notes/2018/2018.3.html>`__
    * 2018.2 released June 14th, 2018
        + `Download <2018.2/download.html>`__
        + `Release Notes <2018.2/release-notes/2018/2018.2.html>`__
    * 2018.1 released March 21st, 2018
        + `Download <2018.1/download.html>`__
        + `Release Notes <2018.1/release-notes/2018/2018.1.html>`__
    * 2018 released January 10th, 2018
        + `Download <2018/download.html>`__
        + `Release Notes <2018/release-notes/index.html>`__

* GROMACS 2016 series

    `Documentation <2016-current/index.html>`__ for the final version.

    * 2016.6 released February 8th, 2019
        + `Download <2016.6/download.html>`__
        + `Release Notes <2016.6/ReleaseNotes/index.html>`__
    * 2016.5 released February 16th, 2018
        + `Download <2016.5/download.html>`__
        + `Release Notes <2016.5/ReleaseNotes/index.html>`__
    * 2016.4 released September 15th, 2017
        + `Download <2016.4/download.html>`__
        + `Release Notes <2016.4/ReleaseNotes/index.html>`__
    * 2016.3 released March 14th, 2017
        + `Download <2016.3/download.html>`__
        + `Release Notes <2016.3/ReleaseNotes/index.html>`__
    * 2016.2 released February 7th, 2017
        + `Download <2016.2/download.html>`__
        + `Release Notes <2016.2/ReleaseNotes/index.html>`__
    * 2016.1 released October 28th, 2016
        + `Download <2016.1/download.html>`__
        + `Release Notes <2016.1/ReleaseNotes/index.html>`__
    * 2016 released August 4th, 2016
        + `Download <2016/download.html>`__
        + `Release Notes <2016/ReleaseNotes/index.html>`__

* GROMACS 5.1 series

    `Documentation <5.1-current/index.html>`__ for the final version.

    * 5.1.5 - released December 21st, 2017
        + `Download <5.1.5/download.html>`__
        + `Release Notes <5.1.5/ReleaseNotes/index.html>`__
    * 5.1.4 - released September 8th, 2016
        + `Download <5.1.4/download.html>`__
        + `Release Notes <5.1.4/ReleaseNotes/index.html>`__
    * 5.1.3 - released July 21st, 2016
        + `Download <5.1.3/download.html>`__
        + `Release Notes <5.1.3/ReleaseNotes/index.html>`__
    * 5.1.2 - released February 3rd, 2016
        + `Download <5.1.2/download.html>`__
        + `Release Notes <5.1.2/ReleaseNotes/index.html>`__
    * 5.1.1 - released November 9th, 2015
        + `Download <5.1.1/download.html>`__
        + `Release Notes <5.1.1/ReleaseNotes/index.html>`__
    * 5.1 - released August 14th, 2015
        + `Download <5.1/download.html>`__
        + `Release Notes <5.1/ReleaseNotes/index.html>`__

* GROMACS 5.0 series
    * 5.0.7
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.7.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.7/online.html>`__
    * 5.0.6
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.6.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.6/online.html>`__
    * 5.0.5
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.5.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.5/online.html>`__
    * 5.0.4
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.4.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.4/online.html>`__
    * 5.0.3
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.3.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.3/online.html>`__
    * 5.0.2
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.2.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.2/online.html>`__
    * 5.0.1
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.1.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0.1/online.html>`__
    * 5.0
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-5.0.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/5.0/online.html>`__

* GROMACS 4.6 series
    * 4.6.7
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.7.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6.7/online.html>`__
    * 4.6.6
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.6.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6.6/online.html>`__
    * 4.6.5
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.5.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6.5/online.html>`__
    * 4.6.4
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.4.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6.4/online.html>`__
    * 4.6.3
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.3.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6.3/online.html>`__
    * 4.6.2
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.2.pdf>`__
    * 4.6.1
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.1.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6.1/online.html>`__
    * 4.6
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.6.pdf>`__
        + `Online manual <http://manual.gromacs.org/archive/4.6/online.html>`__

* GROMACS 4.5 series
    * `Online manual <http://manual.gromacs.org/archive/4.5/online.html>`__
    * 4.5.6
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.5.6.pdf>`__
    * 4.5.4
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.5.4.pdf>`__
    * 4.5.3
        + `PDF Reference manual <https://ftp.gromacs.org/pub/manual/manual-4.5.3.pdf>`__

For older versions, have a look at https://ftp.gromacs.org/pub/manual/ and https://ftp.gromacs.org/gromacs/.

.. _branch-maintenance-policy:

Branch maintenance policy
-------------------------------

Two versions of GROMACS are under active maintenance, the 2022 series and
the 2021 series. In the latter, only highly conservative fixes will be made,
and only to address issues that affect scientific correctness. Naturally,
some of those releases will be made after the year 2021 ends, but we keep
2021 in the name so users understand how up to date their version is. Such
fixes will also be incorporated into the 2022 release series, as appropriate.
Around the time the 2023 release is made, the 2021 series will no longer be maintained.
